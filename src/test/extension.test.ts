import * as assert from 'assert';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
import { parseDuration } from '../compute-duration';
// import * as myExtension from '../../extension';

suite('Extension Test Suite', () => {
	vscode.window.showInformationMessage('Start all tests.');

	test('Compute Duration Test', () => {
		assert.strictEqual(parseDuration("1h30m"), 90);
		assert.strictEqual(parseDuration("1h30m 30m"), 120);
		assert.strictEqual(parseDuration("1h30m -30m"), 60);
		assert.strictEqual(parseDuration("1h30m -30"), 90);
		assert.strictEqual(parseDuration("  1h 30m -30'   "), 60);
		assert.strictEqual(parseDuration("-1h30m  30' 10''"), -59.833333333333336);
		assert.strictEqual(parseDuration("1h30"), 90);
		assert.strictEqual(parseDuration("1h30m"), 90);
		assert.strictEqual(parseDuration("-1h30"), -90);
	});
});
