import * as vscode from 'vscode';

let debugOutput: vscode.OutputChannel; 
let debugEnabled: boolean;
let debugInitialized = false;

// Load the configuration from Settings...
function loadConfiguration(): void {
	debugEnabled = true; // !!vscode.workspace.getConfiguration('compute-durationrunner.debug').get('enabled'); 
	debugInitialized = true;
}

// Debug output (OUTPUT > Compute-Duration)
export function debug(dbgString: string, show: boolean = false): void {
	if (!debugInitialized) {
        loadConfiguration();
    }
    
    if (!debugEnabled) {
		return;
	}
	
	if (!debugOutput) {
		debugOutput = vscode.window.createOutputChannel("Compute-Duration");
	}
	debugOutput.appendLine(dbgString);
    if (show) {
		debugOutput.show();
	}
}

