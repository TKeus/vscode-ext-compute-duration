import * as vscode from 'vscode';
import { debug } from './debug';

const ms = 1;
const s = 1000 * ms;
const m = 60 * s;
const h = 60 * m;
const d = 24 * h;
const w = 7 * d;
const b = d * (365.25 / 12);
const y = d * 365.25;

const duration = {
  ns: 1 / 1e6, nanosecond: 1 / 1e6,
  'μs': 1 / 1e3, us: 1 / 1e3, microsecond: 1 / 1e3,
  ms, millisecond: ms,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  s, sec: s, second: s, "''": s,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  m, min: m, minute: m, "'": m,
  h, hr: h, hour: h,
  d, day: d,
  w, wk: w, week: w,
  b, month: b,
  y, yr: y, year: y
};

var badParsing: string = '';

/**
 * Returns the ratio for a given unit. 
 * @param str A string representing a unit
 * @returns The corresponding unit ratio
 * 
 */
function unitRatio(str: string): number {
  return duration[str.toLowerCase() as keyof typeof duration];
}

/**
* Splits a string into multiple words.
* @param inputText A string to be split
* @returns The corresponding words forming the {@linkcode inputText}
* 
*/
function splitInput(inputText: string) {
  return inputText.split(/\s+/);
}

/**
* Parses a string containing a duration.
* @param str A string to be parsed
* @param format The unit of the duration to be returned
* @returns The duration converted in the unit specified in {@linkcode format}
* 
*/
export function parseDuration(str: string = '', format: string = 'm'): number {
  var result: number = 0;
  var prevUnitRatio: number;

  badParsing = '';

  // ignore commas/placeholders
  str = (str + '').replace(/(\d)[,_](\d)/g, '$1$2');
  debug(`str: <${str}>`);

  // Split into words
  var parts = splitInput(str);
  parts.map((part: string) => {
    // add each part converted in output unit to the result
    debug(`part: <${part}>`);
    // a negative sign will substract the duration
    prevUnitRatio = 0;
    const isNegative: boolean = part.startsWith('-');
    part.replace(/(-?(?:\d+\.?\d*|\d*\.?\d+)(?:e[-+]?\d+)?)\s*([a-zA-Z']*)/ug, function (_, n, units): string {
      debug(`-> part: ${n} <${units}:${unitRatio(units)}>`);
      units = unitRatio(units);
      if ((units === undefined) && (prevUnitRatio === h)) { // no units after hour results in minutes
          units = m;
      }
      prevUnitRatio = units;
      if (units) {
          result = result + Math.abs(parseFloat(n)) * units * (isNegative ? -1 : 1);
      } else {
        badParsing += badParsing !== '' ? ' ' + _ : _;
      }
      return "";
    });
  });
  // Execute final conversion
  if (result && unitRatio(format)) {
    result = result / unitRatio(format);
  }
  return result;
}

/**
* Computes the total duration on the selected text.
*/
export function computeDuration() {
  var selection, totalDurationStr: string;
  var totalDuration, hoursDuration, minutesDuration: number;

  const activeEditor = vscode.window.activeTextEditor;
  if (!activeEditor) {
    return;
  }

  // Get selection 
  selection = activeEditor.document.getText(activeEditor.selection);
  
  // Compute duration equivalent
  totalDuration = parseDuration(selection);
  const isNegative: boolean = (totalDuration < 0);
  totalDuration = Math.abs(totalDuration);
  hoursDuration = Math.trunc(totalDuration/60);
  minutesDuration = totalDuration%60;
  totalDurationStr = (isNegative ? '-':'') + (hoursDuration !== 0 ? `${hoursDuration}h` : '') + (minutesDuration !== 0 ? `${minutesDuration}'` : '') || '0m';
  debug(`Compute Duration OK! ${selection} -> ${totalDurationStr}`);

  // Replace the selection with the computed duration
  activeEditor.edit(editBuilder => {
      editBuilder.replace(activeEditor.selection, badParsing !== '' ? `${totalDurationStr} (${badParsing})` : totalDurationStr);
  });
};
